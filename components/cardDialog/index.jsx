import React from 'react'
function getStyle(style, snapshot) {
  if (!snapshot.isDragging) {
    return {
      ...style,
      transform: 'inherit !important'
    }
  }
  return {
    ...style,
    transform: style.transform
  };
}

const CardDialog = ({ provided, data, snapshot }) => {
  return (
    <div
      ref={provided.innerRef}
      {...provided.draggableProps}
      {...provided.dragHandleProps}
      className='card-dialog'
      // isdragging={snapshot.isDragging && !snapshot.isDropAnimating}
      style={snapshot.isDragging && !snapshot.isDropAnimating ? getStyle(provided.draggableProps.style, snapshot) : {}}
    >
      <img src={data.img} alt={data.alt} />
      <p>{data.text}</p>
    </div>
  )
}

export default CardDialog
