const select = `
<select
    jcr:primaryType="nt:unstructured"
    fieldLabel="Select"
    name="./select"
    required="{Boolean}true"
    sling:resourceType="granite/ui/components/foundation/form/select">
    <items jcr:primaryType="nt:unstructured">
        <optionone
            jcr:primaryType="nt:unstructured"
            text="Option1"
            value="new"
            selected="true"
        />
        <optiontwo
            jcr:primaryType="nt:unstructured"
            text="Option2"
            value="old"
        />
    </items>
</select>`

export default select
