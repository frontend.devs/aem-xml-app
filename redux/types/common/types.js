const COMMON = "COMMON_";

export const PERFORMING_SERVER_REQUEST                  = COMMON + 'PERFORMING_SERVER_REQUEST';
export const UPDATE_INFO_BANNER                         = COMMON + 'UPDATE_INFO_BANNER';
export const SET_CURRENT_USER                           = COMMON + 'SET_CURRENT_USER';
export const GET_DEPARTMENTS                            = COMMON + 'GET_DEPARTMENTS';
export const GET_PROVINCES_BY_DEPARTMENT                = COMMON + 'GET_PROVINCES_BY_DEPARTMENT';
export const GET_DISTRICTS_BY_PROVINCE                  = COMMON + 'GET_DISTRICTS_BY_PROVINCE';
export const GET_TYPES_OF_ROAD                          = COMMON + 'GET_TYPES_OF_ROAD';
export const SHOW_MODAL                                 = COMMON + 'SHOW_MODAL';
export const HIDE_MODAL                                 = COMMON + 'HIDE_MODAL';