const COMMON = "PROPERTY_";

export const SET_TYPE                  = COMMON + 'SET_TYPE';

export const TYPE_TEXTFIELD = 'TEXTFIELD'
export const TYPE_RICHTEXT = 'RICHTEXT'
export const TYPE_SELECT = 'SELECT'
export const TYPE_FILE = 'FILE'
export const TYPE_PATHBROWSER = 'PATHBROWSER'
export const TYPE_CHECKBOX = 'CHECKBOX'