import React, { useState } from 'react'

const LayoutRight = (props) => {
  return <div className='layout__right'>{props.children}</div>
}

export default LayoutRight
