import React from 'react'
import TextField from '../commons/TextField'
const TextFieldDuo = (props) => {
  return (
    <div className='groupInputSelect'>
    <div className='groupInputSelect__item'>
      <TextField
        id="name"
        name={props.nameField}
        handleFocus={this.handleFocus}
        validTextfield=''
        handleBlur={this.handleBlur}
        type="text"
        label="Nombre del campo"
        required="false"
      />
    </div>
    <div className='groupInputSelect__item'>
      <TextField
        id="name"
        name={props.valueField}
        handleFocus={this.handleFocus}
        validTextfield=''
        handleBlur={this.handleBlur}
        type="text"
        label="Valor del campo"
        required="false"
      />
    </div>
    <div className='groupInputSelect__action' />
  </div>
  )
}

export default TextFieldDuo
