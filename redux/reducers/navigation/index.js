import * as types from '../../types/navigation/types'

const initialState = {
  active: types.TYPE_CAMPOS
}

export default (state = initialState, action) => {
  switch(action.type) {
    case types.SET_ACTIVE:
      return {
        active: action.payload
      }
    default:
      return state;
  }
}