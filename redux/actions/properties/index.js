import * as types from '../../types/properties/types'

export const setPropertyType = (type,id) => async dispatch => {
  dispatch({
    type: types.SET_TYPE,
    payload: type,
    id:id
  })
}
