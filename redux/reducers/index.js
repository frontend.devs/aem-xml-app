import { combineReducers } from 'redux';

/**
 * Reducers
 */

import example from "./example/index";
import navigation from './navigation'
import properties from './properties'

const appReducer = combineReducers({
  example,
  navigation,
  properties,
});

const rootReducer = (state, action) => {
  if (action.type === 'USER_LOGGED_OUT') {
    state = undefined;
  }
  return appReducer(state, action);
};

export default rootReducer;
