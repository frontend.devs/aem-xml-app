import * as types from '../../types/properties/types'

const initialState = {
  type: null,
  id:null
}

export default (state = initialState, action) => {
  switch(action.type) {
    case types.SET_TYPE:
      return {
        type: action.payload,
      }
    default:
      return state;
  }
}