// @flow
import React, { Component } from 'react';
import memoizeOne from 'memoize-one';
import { Droppable } from 'react-beautiful-dnd';
import Task from './task';

const colors = {
  N10: 'yellow',
  B200: 'black',
  B50: 'purple',
  N900: 'orange',
  N100: 'red',
  N50: 'blue',
  N200: 'green',
  N0: 'grey'
}

const Container = {
  width: '300px',
  margin: '20px',
  borderRadius: '3px',
  border: '1px solid red',
  backgroundColor: 'blue',
  display: 'flex',
  flexDirection: 'column',
}

const TaskList = {
  padding: '20px',
  minHeight: '200px',
  flexGrow: '1',
  transition: 'background-color 0.2s ease'
}

const getSelectedMap = memoizeOne((selectedTaskIds) =>
  selectedTaskIds.reduce((previous, current) => {
    previous[current] = true;
    return previous;
  }, {}),
);

export default class Column extends Component {
  render() {
    const column = this.props.column;
    const tasks = this.props.tasks;
    const selectedTaskIds = this.props.selectedTaskIds;
    const draggingTaskId = this.props.draggingTaskId;
    return (
      <div style={Container}>
        <div>{column.title}</div>
        <Droppable droppableId={column.id}>
          {(provided, snapshot) => (
            <div
              style={TaskList}
              ref={provided.innerRef}
              isDraggingOver={snapshot.isDraggingOver}
              {...provided.droppableProps}
            >
              {tasks.map((task, index) => {
                const isSelected = Boolean(
                  getSelectedMap(selectedTaskIds)[task.id],
                );
                const isGhosting =
                  isSelected &&
                  Boolean(draggingTaskId) &&
                  draggingTaskId !== task.id;
                return (
                  <Task
                    task={task}
                    index={index}
                    key={task.id}
                    isSelected={isSelected}
                    isGhosting={isGhosting}
                    selectionCount={selectedTaskIds.length}
                    toggleSelection={this.props.toggleSelection}
                    toggleSelectionInGroup={this.props.toggleSelectionInGroup}
                    multiSelectTo={this.props.multiSelectTo}
                  />
                );
              })}
              {provided.placeholder}
            </div>
          )}
        </Droppable>
      </div>
    );
  }
}
