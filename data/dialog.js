import textField from './cards/textfield'
import richText from './cards/richtext'
import select from './cards/select'
import file from './cards/file'
import pathbrowser from './cards/pathbrowser'
import checkbox from './cards/checkbox'
import * as types from '../redux/types/properties/types'

const ITEMS = [
  {
    img: './cardDialog/text-field.png',
    text: 'Texfield',
    alt: 'Textfield',
    content: textField,
    type: types.TYPE_TEXTFIELD,
    config: {
      name: '',
      attributeName: ''
    }
  },
  {
    img: './cardDialog/rich-text.png',
    text: 'Rich Text',
    alt: 'Rich Text',
    content: richText,
    type: types.TYPE_RICHTEXT,
  },
  {
    img: './cardDialog/select.png',
    text: 'Select',
    alt: 'Select',
    content: select,
    type: types.TYPE_SELECT,
  },
  {
    img: './cardDialog/file.png',
    text: 'File',
    alt: 'File',
    content: file,
    type: types.TYPE_FILE,
  },
  {
    img: './cardDialog/pathbrowser.png',
    text: 'Pathbrowser',
    alt: 'Pathbrowser',
    content: pathbrowser,
    type: types.TYPE_PATHBROWSER,
  },
  {
    img: './cardDialog/checkbox.png',
    text: 'Checkbox',
    alt: 'Checkbox',
    content: checkbox,
    type: types.TYPE_CHECKBOX,
  },
]

export default ITEMS
