import React from 'react'
import Nav from '../Nav'

const CardDialogContainer = ({ children }) => {
  return (
    <>
      <Nav />
      <div className='card-dialog-container'>
        { children }
      </div>
    </>
  )
}

const CardDialogWrap = (props) => {
  return (
    <div
      className='card-dialog-container-wrap'
      ref={props.provided.innerRef}
    >
      {props.children}
    </div>
  )
}

export {
  CardDialogContainer,
  CardDialogWrap
}
