import { createStore, applyMiddleware } from 'redux';
import { createLogger } from 'redux-logger';
import thunk from 'redux-thunk';
import reducers from './reducers/index';
import { composeWithDevTools } from 'redux-devtools-extension';
import { checkPendingActions } from '../utils/actionsDependencies';

const middlewares = [];

const actionsDependencies = store => next => action => {
  next(action);
  checkPendingActions(store.getState());
}

middlewares.push(actionsDependencies);
middlewares.push(thunk);

const store = composeWithDevTools(applyMiddleware(...middlewares))(createStore)(reducers);

export default store;
