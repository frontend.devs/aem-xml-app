import { TYPE_TEXTFIELD, TYPE_RICHTEXT, TYPE_SELECT, TYPE_FILE, TYPE_PATHBROWSER, TYPE_CHECKBOX } from '../redux/types/properties/types'

const indent = (string, idents, opt_spacesPerIndent) => {
  let numOfIndents = idents
  let str = string
  str = str.replace(/^(?=.)/gm, new Array(numOfIndents + 1).join('\t'))
  numOfIndents = new Array(opt_spacesPerIndent + 1 || 0).join(' ') // re-use
  return opt_spacesPerIndent
    ? str.replace(/^\t+/g, (tabs) => {
        return tabs.replace(/./g, numOfIndents)
      })
    : str
}
const FormatTextfield = (name, attribute) => {
  return `<${name} jcr:primaryType="nt:unstructured" sling:resourceType="granite/ui/components/coral/foundation/form/textfield" fieldLabel="${name}" name="./${attribute}" useFixedInlineToolbar="{Boolean}true"/>`
}
const FormatRich = (name, attribute, h3, h4, h5, h6, span) => {
  return `<${name}
  jcr:primaryType="nt:unstructured"
  sling:resourceType="cq/gui/components/authoring/dialog/richtext"
  fieldLabel="${name}"
  name="./${attribute}"
  useFixedInlineToolbar="{Boolean}true">
  <rtePlugins jcr:primaryType="nt:unstructured">
      <format
          jcr:primaryType="nt:unstructured"
          features="bold,italic"/>
      <justify
          jcr:primaryType="nt:unstructured"
          features="-"/>
      <links
          jcr:primaryType="nt:unstructured"
          features="modifylink,unlink"/>
      <lists
          jcr:primaryType="nt:unstructured"
          features="*"/>
      <misctools jcr:primaryType="nt:unstructured">
          <specialCharsConfig jcr:primaryType="nt:unstructured">
              <chars jcr:primaryType="nt:unstructured">
                  <default_copyright
                      jcr:primaryType="nt:unstructured"
                      entity="&amp;copy;"
                      name="copyright"/>
                  <default_euro
                      jcr:primaryType="nt:unstructured"
                      entity="&amp;euro;"
                      name="euro"/>
                  <default_registered
                      jcr:primaryType="nt:unstructured"
                      entity="&amp;reg;"
                      name="registered"/>
                  <default_trademark
                      jcr:primaryType="nt:unstructured"
                      entity="&amp;trade;"
                      name="trademark"/>
              </chars>
          </specialCharsConfig>
      </misctools>
      <paraformat
          jcr:primaryType="nt:unstructured"
          features="*">
          <formats jcr:primaryType="nt:unstructured">
              <default_p
                  jcr:primaryType="nt:unstructured"
                  description="Parrafo"
                  tag="p"/>
              ${h3 || ''}
              ${h4 || ''}
              ${h5 || ''}
              ${h6 || ''}
              ${span || ''}
          </formats>
      </paraformat>
      <table
          jcr:primaryType="nt:unstructured"
          features="-">
          <hiddenHeaderConfig
              jcr:primaryType="nt:unstructured"
              hiddenHeaderClassName="cq-wcm-foundation-aria-visuallyhidden"
              hiddenHeaderEditingCSS="cq-RichText-hiddenHeader--editing"/>
      </table>
      <tracklinks
          jcr:primaryType="nt:unstructured"
          features="*"/>
  </rtePlugins>
  <uiSettings jcr:primaryType="nt:unstructured">
      <cui jcr:primaryType="nt:unstructured">
          <inline
              jcr:primaryType="nt:unstructured"
              toolbar="[format#bold,format#italic,format#underline,#justify,#lists,links#modifylink,links#unlink,#paraformat]">
              <popovers jcr:primaryType="nt:unstructured">
                  <justify
                      jcr:primaryType="nt:unstructured"
                      items="[justify#justifyleft,justify#justifycenter,justify#justifyright]"
                      ref="justify"/>
                  <lists
                      jcr:primaryType="nt:unstructured"
                      items="[lists#unordered,lists#ordered,lists#outdent,lists#indent]"
                      ref="lists"/>
                  <paraformat
                      jcr:primaryType="nt:unstructured"
                      items="paraformat:getFormats:paraformat-pulldown"
                      ref="paraformat"/>
              </popovers>
          </inline>
          <dialogFullScreen
              jcr:primaryType="nt:unstructured"
              toolbar="[format#bold,format#italic,format#underline,justify#justifyleft,justify#justifycenter,justify#justifyright,lists#unordered,lists#ordered,lists#outdent,lists#indent,links#modifylink,links#unlink,table#createoredit,#paraformat,image#imageProps]">
              <popovers jcr:primaryType="nt:unstructured">
                  <paraformat
                      jcr:primaryType="nt:unstructured"
                      items="paraformat:getFormats:paraformat-pulldown"
                      ref="paraformat"/>
              </popovers>
          </dialogFullScreen>
          <tableEditOptions
              jcr:primaryType="nt:unstructured"
              toolbar="[table#insertcolumn-before,table#insertcolumn-after,table#removecolumn,-,table#insertrow-before,table#insertrow-after,table#removerow,-,table#mergecells-right,table#mergecells-down,table#mergecells,table#splitcell-horizontal,table#splitcell-vertical,-,table#selectrow,table#selectcolumn,-,table#ensureparagraph,-,table#modifytableandcell,table#removetable,-,undo#undo,undo#redo,-,table#exitTableEditing,-]">
          </tableEditOptions>
      </cui>
  </uiSettings>
  </${name}>`
}
const FormatFile = (name, attribute, allow) => {
  return `  <${name}
  jcr:primaryType="nt:unstructured"
  sling:resourceType="cq/gui/components/authoring/dialog/fileupload"
  allowUpload="{Boolean}${!!allow}"
  autoStart="false"
  class="cq-droptarget"
  fieldLabel="${name}"
  fileNameParameter="./fileName"
  fileReferenceParameter="./file"
  mimeTypes="[image]"
  multiple="{Boolean}false"
  title="Upload Image Asset"
  uploadUrl="\${suffix.path}"
  useHTML5="true"
  name="./${attribute}"
/>`
}
const FormatPath = (name, attribute, base) => {
  return `<${attribute}
  jcr:primaryType="nt:unstructured"
  sling:resourceType="granite/ui/components/coral/foundation/form/pathbrowser"
  fieldLabel="${name}"
  name="./${attribute}"
  rootPath="${base}"
  useFixedInlineToolbar="{Boolean}true"/>`
}
const FormatSelect = (name, attribute, items) => {
  return `<${name}
  jcr:primaryType="nt:unstructured"
  fieldLabel="${name}"
  name="./${attribute}"
  required="{Boolean}true"
  sling:resourceType="granite/ui/components/foundation/form/select">
  <items jcr:primaryType="nt:unstructured">
    ${items.map(
      (e, index) => `<option${index}
        jcr:primaryType="nt:unstructured"
        text="${e.fieldName}"
        value="${e.fieldValue}"
        selected="${index === 0}"
      />`
    )}
  </items>
</${name}>`
}
const FormatCheckbox = (name, attribute, checked = false) => {
  return `<${name}
    jcr:primaryType="nt:unstructured"
    sling:resourceType="granite/ui/components/foundation/form/checkbox"
    text="Yes"
    uncheckedValue="false"
    value="{Boolean}${checked}"
    name="./${attribute}"
  />`
}

const getIconType = (item) => {
  switch (item) {
    case TYPE_TEXTFIELD:
      return 'text'
    case TYPE_RICHTEXT:
      return 'richText'
    case TYPE_SELECT:
      return 'select'
    case TYPE_FILE:
      return 'file'
    case TYPE_PATHBROWSER:
      return 'pathBrowser'
    case TYPE_CHECKBOX:
      return 'checkbox'
    default:
      return ''
  }
}

const handleFocus = (e) => {
  console.log('focus')
  e.target.parentNode.children[1].classList.add('active')
}

const handleBlur = (e) => {
  console.log('blur')
  if (e.target.value.length > 0) {
    e.target.parentNode.children[1].classList.add('active')
  } else {
    e.target.parentNode.children[1].classList.remove('active')
  }
}

const styleModal = () => {
  return {
    overlay: {
      backgroundColor: 'rgba(35, 45, 70, 0.8)',
      zIndex: '999',
    },
    content: {
      top: '50%',
      left: '50%',
      right: 'auto',
      bottom: 'auto',
      marginRight: '-50%',
      transform: 'translate(-50%, -50%)',
    },
  }
}

export { indent, getIconType, handleFocus, handleBlur, styleModal, FormatRich, FormatTextfield, FormatFile, FormatPath, FormatSelect, FormatCheckbox }
