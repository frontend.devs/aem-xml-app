const file = `
<file
    jcr:primaryType="nt:unstructured"
    sling:resourceType="cq/gui/components/authoring/dialog/fileupload"
    allowUpload="{Boolean}false"
    autoStart="false"
    class="cq-droptarget"
    fieldLabel="File"
    fileNameParameter="./fileName"
    fileReferenceParameter="./fileReference"
    mimeTypes="[image]"
    multiple="{Boolean}false"
    name="./file"
    title="Upload Image Asset"
    uploadUrl=""
    useHTML5="true"
/>`

export default file
