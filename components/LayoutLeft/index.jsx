import React, { useState } from 'react'

const LayoutLeft = (props) => {
  return <div className='layout__left'>{props.children}</div>
}

export default LayoutLeft
