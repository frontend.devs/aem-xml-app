 import * as types from '../../types/common/types';

const initialState = {
    exampleProps : {}
}

export default (state = initialState, action) => {
    switch(action.type) {
        case types.SHOW_MODAL:
            return {
                exampleProps:action.payload
            }
        case types.HIDE_MODAL:
            return initialState
        default:
            return state;
    }
}