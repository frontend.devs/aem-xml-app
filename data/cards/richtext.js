const richText = `
<richtext
    jcr:primaryType="nt:unstructured"
    sling:resourceType="cq/gui/components/authoring/dialog/richtext"
    fieldLabel="Texto del título"
    name="./richtext"
    useFixedInlineToolbar="{Boolean}true">
    <rtePlugins jcr:primaryType="nt:unstructured">
        <format
            jcr:primaryType="nt:unstructured"
            features="bold,italic"/>
        <justify
            jcr:primaryType="nt:unstructured"
            features="-"/>
        <links
            jcr:primaryType="nt:unstructured"
            features="modifylink,unlink"/>
        <lists
            jcr:primaryType="nt:unstructured"
            features="*"/>
        <misctools jcr:primaryType="nt:unstructured">
            <specialCharsConfig jcr:primaryType="nt:unstructured">
                <chars jcr:primaryType="nt:unstructured">
                    <default_copyright
                        jcr:primaryType="nt:unstructured"
                        entity="&amp;copy;"
                        name="copyright"/>
                    <default_euro
                        jcr:primaryType="nt:unstructured"
                        entity="&amp;euro;"
                        name="euro"/>
                    <default_registered
                        jcr:primaryType="nt:unstructured"
                        entity="&amp;reg;"
                        name="registered"/>
                    <default_trademark
                        jcr:primaryType="nt:unstructured"
                        entity="&amp;trade;"
                        name="trademark"/>
                </chars>
            </specialCharsConfig>
        </misctools>
        <paraformat
            jcr:primaryType="nt:unstructured"
            features="*">
            <formats jcr:primaryType="nt:unstructured">
                <default_p
                    jcr:primaryType="nt:unstructured"
                    description="Parrafo"
                    tag="p"/>
                <default_h3
                    jcr:primaryType="nt:unstructured"
                    description="Título de Acceso directo"
                    tag="h3"/>
                <default_h4
                    jcr:primaryType="nt:unstructured"
                    description="Título de Novedades"
                    tag="h4"/>
                <default_h2
                    jcr:primaryType="nt:unstructured"
                    description="Titulo"
                    tag="h2"/>
            </formats>
        </paraformat>
        <table
            jcr:primaryType="nt:unstructured"
            features="-">
            <hiddenHeaderConfig
                jcr:primaryType="nt:unstructured"
                hiddenHeaderClassName="cq-wcm-foundation-aria-visuallyhidden"
                hiddenHeaderEditingCSS="cq-RichText-hiddenHeader--editing"/>
        </table>
        <tracklinks
            jcr:primaryType="nt:unstructured"
            features="*"/>
    </rtePlugins>
    <uiSettings jcr:primaryType="nt:unstructured">
        <cui jcr:primaryType="nt:unstructured">
            <inline
                jcr:primaryType="nt:unstructured"
                toolbar="[format#bold,format#italic,format#underline,#justify,#lists,links#modifylink,links#unlink,#paraformat]">
                <popovers jcr:primaryType="nt:unstructured">
                    <justify
                        jcr:primaryType="nt:unstructured"
                        items="[justify#justifyleft,justify#justifycenter,justify#justifyright]"
                        ref="justify"/>
                    <lists
                        jcr:primaryType="nt:unstructured"
                        items="[lists#unordered,lists#ordered,lists#outdent,lists#indent]"
                        ref="lists"/>
                    <paraformat
                        jcr:primaryType="nt:unstructured"
                        items="paraformat:getFormats:paraformat-pulldown"
                        ref="paraformat"/>
                </popovers>
            </inline>
            <dialogFullScreen
                jcr:primaryType="nt:unstructured"
                toolbar="[format#bold,format#italic,format#underline,justify#justifyleft,justify#justifycenter,justify#justifyright,lists#unordered,lists#ordered,lists#outdent,lists#indent,links#modifylink,links#unlink,table#createoredit,#paraformat,image#imageProps]">
                <popovers jcr:primaryType="nt:unstructured">
                    <paraformat
                        jcr:primaryType="nt:unstructured"
                        items="paraformat:getFormats:paraformat-pulldown"
                        ref="paraformat"/>
                </popovers>
            </dialogFullScreen>
            <tableEditOptions
                jcr:primaryType="nt:unstructured"
                toolbar="[table#insertcolumn-before,table#insertcolumn-after,table#removecolumn,-,table#insertrow-before,table#insertrow-after,table#removerow,-,table#mergecells-right,table#mergecells-down,table#mergecells,table#splitcell-horizontal,table#splitcell-vertical,-,table#selectrow,table#selectcolumn,-,table#ensureparagraph,-,table#modifytableandcell,table#removetable,-,undo#undo,undo#redo,-,table#exitTableEditing,-]">
            </tableEditOptions>
        </cui>
    </uiSettings>
</richtext>`

export default richText
