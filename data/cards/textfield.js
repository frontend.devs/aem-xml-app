const textField = `
<textfield
    jcr:primaryType="nt:unstructured"
    sling:resourceType="granite/ui/components/coral/foundation/form/textfield"
    fieldLabel="Textfield"
    name="./textfield"
    useFixedInlineToolbar="{Boolean}true"
/>`

export default textField
