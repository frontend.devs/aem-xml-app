import * as types from '../../types/navigation/types'

export const setMenuAction = (menu) => async dispatch => {
  dispatch({
    type: types.SET_ACTIVE,
    payload: menu
  })
}
