// @flow
import React, { Component } from 'react';
import { Draggable } from 'react-beautiful-dnd';

// https://developer.mozilla.org/en-US/docs/Web/API/MouseEvent/button
const primaryButton = 0;

const colors = {
  N10: 'yellow',
  B200: 'black',
  B50: 'purple',
  N900: 'orange',
  N100: 'red',
  N50: 'blue',
  N200: 'green',
  N0: 'grey'
}

const getBackgroundColor = ({
  isSelected,
  isGhosting,
}) => {
  if (isGhosting) {
    return colors.N10;
  }

  if (isSelected) {
    return colors.B50;
  }

  return colors.N10;
};

const getColor = ({ isSelected, isGhosting }) => {
  if (isGhosting) {
    return 'darkgrey';
  }
  if (isSelected) {
    return colors.B200;
  }
  return colors.N900;
};

const Container = {
  backgroundColor: 'orange',
  color: 'red',
  padding: '5px',
  marginBottom: '5px',
  borderRadius: '3px',
  fontSize: '18px',
  border: '1px solid ${colors.N90}',
  position: 'relative'
}
/* stylelint-disable block-no-empty */
/* stylelint-enable */
const size = 30;

const SelectionCountStyle = {
  right: '-10px',
  top: '-4px',
  color: 'grey',
  background: 'green',
  borderRadius: '50%',
  height: '20px',
  width: '150px',
  lineHeight: '12px',
  position: 'absolute',
  textAlign: 'center',
  fontSize: '0.8rem',
}

const keyCodes = {
  enter: 13,
  escape: 27,
  arrowDown: 40,
  arrowUp: 38,
  tab: 9,
};

export default class Task extends Component {
  onKeyDown = (
    event,
    provided,
    snapshot,
  ) => {
    if (provided.dragHandleProps) {
      provided.dragHandleProps.onKeyDown(event);
    }

    if (event.defaultPrevented) {
      return;
    }

    if (snapshot.isDragging) {
      return;
    }

    if (event.keyCode !== keyCodes.enter) {
      return;
    }

    // we are using the event for selection
    event.preventDefault();

    this.performAction(event);
  };

  // Using onClick as it will be correctly
  // preventing if there was a drag
  onClick = (event) => {
    if (event.defaultPrevented) {
      return;
    }

    if (event.button !== primaryButton) {
      return;
    }

    // marking the event as used
    event.preventDefault();

    this.performAction(event);
  };

  onTouchEnd = (event) => {
    if (event.defaultPrevented) {
      return;
    }

    // marking the event as used
    // we would also need to add some extra logic to prevent the click
    // if this element was an anchor
    event.preventDefault();
    this.props.toggleSelectionInGroup(this.props.task.id);
  };

  // Determines if the platform specific toggle selection in group key was used
  wasToggleInSelectionGroupKeyUsed = (event) => {
    const isUsingWindows = navigator.platform.indexOf('Win') >= 0;
    return isUsingWindows ? event.ctrlKey : event.metaKey;
  };

  // Determines if the multiSelect key was used
  wasMultiSelectKeyUsed = (event) => event.shiftKey;

  performAction = (event) => {
    const {
      task,
      toggleSelection,
      toggleSelectionInGroup,
      multiSelectTo,
    } = this.props;

    if (this.wasToggleInSelectionGroupKeyUsed(event)) {
      toggleSelectionInGroup(task.id);
      return;
    }

    if (this.wasMultiSelectKeyUsed(event)) {
      multiSelectTo(task.id);
      return;
    }

    toggleSelection(task.id);
  };

  render() {
    const task = this.props.task;
    const index = this.props.index;
    const isSelected = this.props.isSelected;
    const selectionCount = this.props.selectionCount;
    const isGhosting = this.props.isGhosting;
    return (
      <Draggable draggableId={task.id} index={index}>
        {(provided, snapshot) => {
          const shouldShowSelection =
            snapshot.isDragging && selectionCount > 1;

          return (
            <div
              style={Container}
              ref={provided.innerRef}
              {...provided.draggableProps}
              {...provided.dragHandleProps}
              onClick={this.onClick}
              onTouchEnd={this.onTouchEnd}
              onKeyDown={(event) =>
                this.onKeyDown(event, provided, snapshot)
              }
              isDragging={snapshot.isDragging}
              isSelected={isSelected}
              isGhosting={isGhosting}
            >
              <div>{task.content}</div>
              {shouldShowSelection ? (
                <div style={SelectionCountStyle}>{selectionCount}</div>
              ) : null}
            </div>
          );
        }}
      </Draggable>
    );
  }
}
