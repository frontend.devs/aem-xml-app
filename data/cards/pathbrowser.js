const pathbrowser = `
<pathbrowser
    jcr:primaryType="nt:unstructured"
    sling:resourceType="granite/ui/components/coral/foundation/form/pathbrowser"
    fieldLabel="Pathbrowser"
    name="./pathbrowser"
    useFixedInlineToolbar="{Boolean}true"
/>`

export default pathbrowser
