import React from 'react'
import { DragDropContext, Droppable, Draggable, resetServerContext } from 'react-beautiful-dnd'
import { v4 as uuid } from 'uuid'
import Modal from 'react-modal'
import { connect } from 'react-redux'
import LayoutRight from '../components/LayoutRight'
import LayoutLeft from '../components/LayoutLeft'
import Content from '../components/Content'
import { CardDialogContainer, CardDialogWrap } from '../components/cardDialogContainer'
import CardDialog from '../components/cardDialog'
import CardDialogPlaceholder from '../components/cardDialog/placeholder'
import { reorder, copy, moveCopy, moveDelete } from '../utils/drop'
import ITEMS from '../data/dialog'
import head from '../data/cards/head'
import footer from '../data/cards/footer'
import { indent, getIconType, handleFocus, handleBlur, styleModal, FormatRich, FormatTextfield, FormatFile, FormatPath, FormatSelect, FormatCheckbox } from '../utils/functions'
import { TYPE_CAMPOS, TYPE_PROPIEDADES, TYPE_DIALOGOS } from '../redux/types/navigation/types'
import { setMenuAction } from '../redux/actions/navigation'
import { setPropertyType } from '../redux/actions/properties'
import { TYPE_TEXTFIELD, TYPE_RICHTEXT, TYPE_SELECT, TYPE_FILE, TYPE_PATHBROWSER, TYPE_CHECKBOX } from '../redux/types/properties/types'
import TextField from '../components/commons/TextField'
import TitleField from '../components/commons/TitleField'
import Checkbox from '../components/commons/Checkbox'

class Home extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      items: { [uuid()]: [] },
      itemsConfig: [
        {
          name: 'Título',
          edit: false,
          modify: false,
        },
      ],
      selectedItemParent: 0,
      selectedItemID: 0,
      modalIsOpen: false,
      modalType: '',
      fileFields: { name: '' },
      optionsSelectCount: 1
    }
  }

  componentDidMount() {
    const { items, itemsConfig } = this.state
    const keys = Object.keys(items)[0]
    const copyItemsConfig = [...itemsConfig]
    copyItemsConfig[0].id = keys
    this.setState({ itemsConfig: copyItemsConfig })
  }

  onDragEnd = (result) => {
    const { source, destination } = result
    if (!destination) {
      console.log('ITEM DROPEADO FUERA DE ÁREA')
      return
    }
    const copyItems = { ...this.state.items }
    switch (source.droppableId) {
      case destination.droppableId:
        copyItems[destination.droppableId] = reorder(this.state.items[source.droppableId], source.index, destination.index)
        this.setState({ items: copyItems })
        break
      case 'ITEMS':
        copyItems[destination.droppableId] = copy(ITEMS, this.state.items[destination.droppableId], source, destination)
        this.setState(
          {
            items: copyItems,
            selectedItemID: copyItems[destination.droppableId][destination.index].id,
            selectedItemParent: destination.droppableId,
          },
          () => {
            this.props.setMenuAction(TYPE_PROPIEDADES)
            this.props.setPropertyType(ITEMS[source.index].type, ITEMS[source.index])
          }
        )
        break
      default:
        copyItems[source.droppableId] = moveDelete(this.state.items[source.droppableId], this.state.items[destination.droppableId], source, destination)
        copyItems[destination.droppableId] = moveCopy(this.state.items[source.droppableId], this.state.items[destination.droppableId], source, destination)

        this.setState({ items: copyItems })
        break
    }
  }

  // Limpio del state los valores ingresados al cerrar el modal
  onCancelModal = () => {
    this.setState({
      selectedItemParent: 0,
      selectedItemID: 0,
      modalIsOpen: false,
      modalType: '',
    })
  }

  // Acción que se ejecuta al confirmar la acción del modal
  onDeleteItem = () => {
    const { items, itemsConfig, modalType, selectedItemParent, selectedItemID } = this.state

    const copyITems = { ...items }
    const copyItemsConfig = [...itemsConfig]

    if (modalType === 'DELETE-TAB') {
      if (Object.keys(items).length < 2) {
        console.log('NO PUEDES QUEDARTE SIN UN TABBBBBBB')
        return
      }
      const index = copyItemsConfig.findIndex((item) => item.id === selectedItemParent)
      copyItemsConfig.splice(index, 1)
      delete copyITems[selectedItemParent]
    } else if (modalType === 'DELETE-ITEM') {
      const index = copyITems[selectedItemParent].findIndex((childItem) => childItem.id === selectedItemID)
      copyITems[selectedItemParent].splice(index, 1)
    }

    this.setState({
      items: copyITems,
      itemsConfig: copyItemsConfig,
      selectedItemParent: 0,
      selectedItemID: 0,
      modalIsOpen: false,
      modalType: '',
    })
    this.props.setMenuAction(TYPE_CAMPOS)
  }

  // Cuando cancelo el formulario
  onCancelProperty = (e) => {
    e.preventDefault()
    this.props.setMenuAction(TYPE_CAMPOS)
    this.setState({
      selectedItemParent: 0,
      selectedItemID: 0,
    })
  }

  getStyle = (style, snapshot) => {
    const customStyle = style
    if (snapshot.isDragging) {
      return { ...customStyle }
    }
    if (customStyle.transform) {
      customStyle.transform = 'translate(0px, 46px)'
      return { ...customStyle }
    }
    return { ...customStyle }
  }

  getProperties() {
    const { properties } = this.props.global
    let renderFields = ''
    switch (properties.type) {
      case TYPE_TEXTFIELD:
        renderFields = this.renderTextField()
        break
      case TYPE_RICHTEXT:
        renderFields = this.renderRichText()
        break
      case TYPE_SELECT:
        renderFields = this.renderSelect()
        break
      case TYPE_FILE:
        renderFields = this.renderFile()
        break
      case TYPE_PATHBROWSER:
        renderFields = this.renderPathBrowser()
        break
      case TYPE_CHECKBOX:
        renderFields = this.renderCheckbox()
        break
      default:
        renderFields = ''
        break
    }
    return (
      <div>
        <form className='form-field' onSubmit={(e) => this.updateField(e, this.props.global.navigation.active)}>
          {renderFields}
          <div className='form-field__buttons'>
            <button className='btn__primary--border' onClick={() => this.onCancelProperty()}>
              Cancelar
            </button>
            <button className='btn__primary' type='submit'>
              Actualizar
            </button>
          </div>
        </form>
      </div>
    )
  }

  addList = () => {
    const { items, itemsConfig } = this.state
    const copyItems = { ...items }
    const copyItemsConfig = [...itemsConfig]

    copyItems[uuid()] = []

    const arrKeys = Object.keys(copyItems)
    const lastKeyId = arrKeys[arrKeys.length - 1]

    copyItemsConfig.push({
      name: `Título ${Object.keys(items).length + 1}`,
      edit: false,
      modify: false,
      id: lastKeyId,
    })
    this.setState({
      items: copyItems,
      itemsConfig: copyItemsConfig,
    })
  }
  copyDialog = (objects, title = 'Titulo tab') => {
    const content = Object.keys(this.state.items).map((list) => {
      const xml = this.state.items[list].map((item, index) => {
        return item.config.content
      })
      return xml.join('')
    })
    let arrXml = ''
    for (let i = 0; i < content.length; i++) {
      arrXml += `<filters
      jcr:primaryType="nt:unstructured"
      jcr:title="${title[i].name}"
      sling:resourceType="granite/ui/components/coral/foundation/container">
      <items jcr:primaryType="nt:unstructured">${content[i]}
      </items>
    </filters>
    `
    }

    const xmlIdent = indent(arrXml, 12, 4)
    const elementXMLCopy = [head, xmlIdent, footer].join('\n')
    const dialogs = document.createElement('textarea')
    dialogs.innerText = elementXMLCopy
    document.body.appendChild(dialogs)
    dialogs.select()
    document.execCommand('copy')
    dialogs.remove()
  }
  generateXML = (objects, title = 'Titulo tab') => {
    const content = Object.keys(this.state.items).map((list) => {
      const xml = this.state.items[list].map((item, index) => {
        return item.config.content
      })
      return xml.join('')
    })
    let arrXml = ''
    for (let i = 0; i < content.length; i++) {
      arrXml += `<filters
      jcr:primaryType="nt:unstructured"
      jcr:title="${title[i].name}"
      sling:resourceType="granite/ui/components/coral/foundation/container">
      <items jcr:primaryType="nt:unstructured">${content[i]}
      </items>
    </filters>
    `
    }

    const xmlIdent = indent(arrXml, 12, 4)
    const elementXML = [head, xmlIdent, footer].join('\n')
    const bb = new Blob([elementXML], { type: 'text/plain' })
    const pom = document.createElement('a')
    pom.setAttribute('href', window.URL.createObjectURL(bb))
    pom.setAttribute('download', '.content.xml')
    // nombre.xml personalizado
    pom.click()
  }

  getDialogs = () => {
    return <div>dialogos</div>
  }

  // Abro un modal para preguntar si quiero eliminar el tab completo
  onRemoveTab = (parent) => {
    this.setState({
      selectedItemParent: parent,
      selectedItemID: 0,
      modalIsOpen: true,
      modalType: 'DELETE-TAB',
    })
  }

  // Duplico el item en la lista
  duplicateItem = (position, parent, type) => {
    const { items } = this.state
    const copyItems = { ...items }

    const indexItemType = ITEMS.findIndex((item) => item.type === type)

    const source = {
      droppableId: 'ITEMS',
      index: indexItemType,
    }

    const destination = {
      droppableId: parent,
      index: position,
    }

    /*
      Obtengo el item de los objetos según su nombre de propiedad
      y devuelvo un array con el objeto que se quiso duplicar
    */
    copyItems[parent] = copy(ITEMS, copyItems[parent], source, destination)

    this.setState({ items: copyItems })
  }

  // Activa el menu propiedades y muestra la propiedad según su tipo
  onConfigItem = (id, parent, type) => {
    this.setState({
      selectedItemParent: parent,
      selectedItemID: id,
    })
    this.props.setMenuAction(TYPE_PROPIEDADES)
    this.props.setPropertyType(type)
  }

  getCampos = () => {
    return (
      <Droppable droppableId='ITEMS' isDropDisabled>
        {(providedParent, snapshotParent) => (
          <CardDialogWrap provided={providedParent} snapshot={snapshotParent}>
            {ITEMS.map((item, index) => (
              <Draggable key={item.type} draggableId={item.type} index={index}>
                {(provided, snapshot) => {
                  return (
                    <React.Fragment>
                      <CardDialog snapshot={snapshot} provided={provided} data={item} />
                      {snapshot.isDragging && !snapshot.isDropAnimating && <CardDialogPlaceholder data={item} />}
                    </React.Fragment>
                  )
                }}
              </Draggable>
            ))}
          </CardDialogWrap>
        )}
      </Droppable>
    )
  }

  ediTitle(position) {
    const { items, itemsConfig } = this.state

    const uuidKeyValue = Object.keys(items)[position]
    const index = itemsConfig.findIndex((item) => item.id === uuidKeyValue)
    const copyItemsConfig = [...itemsConfig]

    copyItemsConfig[index].modify = true
    copyItemsConfig[index].edit = true

    this.setState({ itemsConfig: copyItemsConfig })
  }

  updateTitle(e, position) {
    e.preventDefault()

    const { items, itemsConfig } = this.state

    const uuidKeyValue = Object.keys(items)[position]
    const index = itemsConfig.findIndex((item) => item.id === uuidKeyValue)
    const copyItemsConfig = [...itemsConfig]

    copyItemsConfig[index].name = e.target.title.value
    copyItemsConfig[index].modify = false
    copyItemsConfig[index].edit = false

    this.setState({ itemsConfig: copyItemsConfig })
  }

  updateField(e, i) {
    e.preventDefault()
    console.log(i)
    const { items, selectedItemParent, selectedItemID } = this.state

    const form = e.target
    ;[...form.elements].forEach((input) => {
      console.log(`${input.name} --> ${input.value}`)
    })

    const { properties } = this.props.global

    const copyItems = { ...items }
    const arrIndex = copyItems[selectedItemParent].findIndex((item) => item.id === selectedItemID)

    switch (properties.type) {
      case TYPE_TEXTFIELD:
        const attributeName = e.target.elements.atributo.value
        const name = e.target.elements.nombre.value
        const formatTextfield = FormatTextfield(name, attributeName)
        copyItems[this.state.selectedItemParent][arrIndex].config = {
          name,
          attributeName,
          content: formatTextfield,
        }
        break
      case TYPE_RICHTEXT:
        const attributeNameRich = e.target.elements.atributo.value
        const nameRich = e.target.elements.nombre.value
        const Richh3 = e.target.elements.allowh3.checked
          ? `<default_h3
             jcr:primaryType='nt:unstructured'
             description='h3'
             tag='h3'/>`
          : false
        const Richh4 = e.target.elements.allowh4.checked
          ? `<default_h4
             jcr:primaryType='nt:unstructured'
             description='h4'
             tag='h4'/>`
          : false
        const Richh5 = e.target.elements.allowh5.checked
          ? `<default_h5
             jcr:primaryType='nt:unstructured'
             description='h5'
             tag='h5'/>`
          : false
        const Richh6 = e.target.elements.allowh6.checked
          ? `<default_h6
             jcr:primaryType='nt:unstructured'
             description='h6'
             tag='h6'/>`
          : false
        const Richspan = e.target.elements.allowspan.checked
          ? `<default_span
             jcr:primaryType='nt:unstructured'
             description='span'
             tag='span'/>`
          : false
        const formatRich = FormatRich(nameRich, attributeNameRich, Richh3, Richh4, Richh5, Richh6, Richspan)
        copyItems[this.state.selectedItemParent][arrIndex].config = {
          name: nameRich,
          attributeName: attributeNameRich,
          content: formatRich,
        }
        break
      case TYPE_SELECT:
        const nameSelect = e.target.elements.nombre.value
        const attributeSelect = e.target.elements.atributo.value
        const fieldsSelect = []
        for (let k = 2; k < [...e.target.elements].length - 2; k += 2) {
          fieldsSelect.push({ fieldName: e.target.elements[k].value, fieldValue: e.target.elements[k + 1].value })
        }
        const formatSelect = FormatSelect(nameSelect, attributeSelect, fieldsSelect)
        copyItems[this.state.selectedItemParent][arrIndex].config = {
          name: nameSelect,
          attributeName: attributeSelect,
          content: formatSelect,
        }
        break
      case TYPE_FILE:
        const nameFile = e.target.elements.nombre.value
        const AttributeFile = e.target.elements.atributo.value
        const formatFile = FormatFile(e.target.elements.nombre.value, e.target.elements.atributo.value, e.target.elements.allowyes.checked)
        copyItems[this.state.selectedItemParent][arrIndex].config = {
          name: nameFile,
          attributeName: AttributeFile,
          content: formatFile,
        }
        break
      case TYPE_PATHBROWSER:
        const namePath = e.target.elements.name.value
        const AttributePath = e.target.elements.attribute.value
        const baseRootPath = e.target.elements.baseRoot.value
        const formatPath = FormatPath(namePath, AttributePath, baseRootPath)
        copyItems[this.state.selectedItemParent][arrIndex].config = {
          name: namePath,
          attributeName: AttributePath,
          content: formatPath,
          baseRootPath,
        }
        break
      case TYPE_CHECKBOX:
        // falta agregar un atributo para saber si estara checked por defecto o no
        const nameCheckbox = e.target.elements.name.value
        const attributeCheckbox = e.target.elements.attribute.value
        const formatCheckbox = FormatCheckbox(nameCheckbox, attributeCheckbox)
        copyItems[this.state.selectedItemParent][arrIndex].config = {
          name: nameCheckbox,
          attributeName: attributeCheckbox,
          content: formatCheckbox,
        }
        break
      default:
        console.log('Error')
    }
    this.setState({ items: copyItems }, () => {
      this.props.setMenuAction(TYPE_CAMPOS)
    })
  }

  openModalDelete(id, parent) {
    this.setState({
      selectedItemParent: parent,
      selectedItemID: id,
      modalIsOpen: true,
      modalType: 'DELETE-ITEM',
    })
  }

  addSelectOptions() {
    console.log('ADDD');
    this.setState({ optionsSelectCount: this.state.optionsSelectCount + 1});
  }

  renderRichText = () => {
    return (
      <>
        <TextField id='nombre' handleFocus={(e) => handleFocus(e)} validTextfield='' handleBlur={(e) => handleBlur(e)} type='text' label='Nombre del campo' required='false' />
        <TextField id='atributo' handleFocus={(e) => handleFocus(e)} validTextfield='' handleBlur={(e) => handleBlur(e)} type='text' label='Nombre del atributo' required='false' />
        <TitleField text='Formatos del Richtext:' />
        <div className='checkbox-container'>
          <Checkbox id='allowh3' checked='false' label='h3' required='false' />
          <Checkbox id='allowh4' checked='false' label='h4' required='false' />
          <Checkbox id='allowh5' checked='false' label='h5' required='false' />
          <Checkbox id='allowh6' checked='false' label='h6' required='false' />
          <Checkbox id='allowspan' checked='false' label='span' required='false' />
        </div>
      </>
    )
  }

  renderSelect = () => {
    const { optionsSelectCount } = this.state;

    return (
      <>
        <TitleField text='Opciones del select:' />
        <TextField id='nombre' handleFocus={(e) => handleFocus(e)} validTextfield='' handleBlur={(e) => handleBlur(e)} type='text' label='Nombre del campo' required='false' />
        <TextField id='atributo' handleFocus={(e) => handleFocus(e)} validTextfield='' handleBlur={(e) => handleBlur(e)} type='text' label='Nombre del atributo' required='false' />
        {
          [...Array(optionsSelectCount)].map((obj, i) => 
          <div className='groupInputSelect' key={i}>
            <div className='groupInputSelect__item'>
              <TextField id={`nombre${i}`} handleFocus={(e) => handleFocus(e)} validTextfield='' handleBlur={(e) => handleBlur(e)} type='text' label='Nombre del campo' required='false' />
            </div>
            <div className='groupInputSelect__item'>
              <TextField id={`valor${i}`} handleFocus={(e) => handleFocus(e)} validTextfield='' handleBlur={(e) => handleBlur(e)} type='text' label='Valor del campo' required='false' />
            </div>
            {(i == optionsSelectCount - 1) && <div className='groupInputSelect__action' onClick={() => this.addSelectOptions()}/>}
          </div>
         )
        }
      </>
    )
  }

  renderFile() {
    return (
      <>
        <TextField id='nombre' onChange={(e) => this.setState({ fileFields: { name: e.target.value.trim() } })} value={this.state.fileFields.name} handleFocus={(e) => handleFocus(e)} validTextfield='' handleBlur={(e) => handleBlur(e)} type='text' label='Nombre del campo' required='false' />
        <TextField id='atributo' handleFocus={(e) => handleFocus(e)} validTextfield='' handleBlur={(e) => handleBlur(e)} type='text' label='Nombre del atributo' required='false' />
        <p className='text-plain'>¿Permitir al usuario cargar imagenes?</p>
        <Checkbox id='allowyes' checked='false' label='Sí' required='false' />
        <Checkbox id='allowno' checked='true' label='No' required='false' />
      </>
    )
  }

  renderPathBrowser = () => {
    return (
      <>
        <TextField id='name' handleFocus={(e) => handleFocus(e)} validTextfield='' handleBlur={(e) => handleBlur(e)} type='text' label='Nombre del campo' required />
        <TextField id='attribute' handleFocus={(e) => handleFocus(e)} validTextfield='' handleBlur={(e) => handleBlur(e)} type='text' label='Nombre del atributo' required />
        <TextField id='baseRoot' handleFocus={(e) => handleFocus(e)} validTextfield='' handleBlur={(e) => handleBlur(e)} type='text' label='Ruta Base' required />
      </>
    )
  }

  renderCheckbox = () => {
    return (
      <>
        <TextField id='name' handleFocus={(e) => handleFocus(e)} validTextfield='' handleBlur={(e) => handleBlur(e)} type='text' label='Nombre del campo' required='false' />
        <TextField id='attribute' handleFocus={(e) => handleFocus(e)} validTextfield='' handleBlur={(e) => handleBlur(e)} type='text' label='Nombre del atributo' required='false' />
      </>
    )
  }

  renderTextField = () => {
    return (
      <React.Fragment>
        <TextField id='nombre' handleFocus={(e) => handleFocus(e)} validTextfield='' handleBlur={(e) => handleBlur(e)} type='text' label='Nombre del campo' />
        <TextField id='atributo' handleFocus={(e) => handleFocus(e)} validTextfield='' handleBlur={(e) => handleBlur(e)} type='text' label='Nombre del atributo' />
      </React.Fragment>
    )
  }

  // Renderizo el tipo de tab según se seleccione
  renderTypeTab = () => {
    console.log(this.props.global)
    const { navigation } = this.props.global
    switch (navigation.active) {
      case TYPE_CAMPOS:
        return this.getCampos()
      case TYPE_PROPIEDADES:
        return this.getProperties()
      case TYPE_DIALOGOS:
        return this.getDialogs()
      default:
        return 'Error'
    }
  }

  render() {
    const { items, itemsConfig } = this.state
    return (
      <React.Fragment>
        <Content>
          <DragDropContext onDragEnd={this.onDragEnd}>
            <LayoutLeft>
              <h2 className='title'>
                <strong>AEM</strong> XML Builder
              </h2>
              <CardDialogContainer>{this.renderTypeTab()}</CardDialogContainer>
            </LayoutLeft>
            <LayoutRight>
              {Object.keys(items).map((list, i) => {
                return (
                  <div className='builder' key={i}>
                    <div className='builder--close' onClick={() => this.onRemoveTab(list)} />
                    <div className='builder--title'>
                      {!itemsConfig[i].modify && itemsConfig[i].name}
                      {!itemsConfig[i].modify && (
                        <button className='builder__edit' onClick={() => this.ediTitle(i)}>
                          <img alt='edit' src='./edit.svg' />
                        </button>
                      )}
                      {itemsConfig[i].edit && (
                        <form onSubmit={(e) => this.updateTitle(e, i)}>
                          <input id='title' name='title' type='text' placeholder={itemsConfig[i].name} />
                          <button className='builder__update' type='submit'>
                            <img alt='update' src='./update.svg' />
                          </button>
                        </form>
                      )}
                    </div>
                    <Droppable key={list} droppableId={list}>
                      {(providedParent, snapshot) => {
                        return (
                          <div className='builder__dropArea' ref={providedParent.innerRef}>
                            {this.state.items[list].length > 0 ? (
                              this.state.items[list].map((item, index) => (
                                <Draggable key={item.id} draggableId={item.id} index={index}>
                                  {(provided, snapshotDragable) => {
                                    // console.log(item)
                                    // isDragging={snapshot.isDragging} style={provided.draggableProps.style}
                                    return (
                                      <React.Fragment>
                                        <div ref={provided.innerRef} {...provided.draggableProps} className='builder__dropArea__component' style={this.getStyle(provided.draggableProps.style, snapshotDragable, provided)} {...provided.dragHandleProps}>
                                          <div className={`builder__dropArea__component__icon builder__dropArea__component__icon--${getIconType(item.type)}`} />
                                          <div className='builder__dropArea__component--text'>
                                            {item.text}
                                            {/* console.log(
                                              item.content,
                                              this.state
                                            ) */}
                                          </div>
                                          <div className='builder__dropArea__component__wrapActions'>
                                            <div className='builder__dropArea__component__wrapActions__actions builder__dropArea__component__wrapActions__actions--copy' onClick={() => this.duplicateItem(index, list, item.type)} />
                                            <div className='builder__dropArea__component__wrapActions__actions builder__dropArea__component__wrapActions__actions--config' onClick={() => this.onConfigItem(item.id, list, item.type)} />
                                            <div className='builder__dropArea__component__wrapActions__actions builder__dropArea__component__wrapActions__actions--delete' onClick={this.openModalDelete.bind(this, item.id, list)} />
                                          </div>
                                        </div>
                                      </React.Fragment>
                                    )
                                  }}
                                </Draggable>
                              ))
                            ) : (
                              <div className='builder__dropArea--placeholder'>Arrastra aquí el campo</div>
                            )}
                            {snapshot.isDraggingOver && this.state.items[list].length > 0 && <div style={{ height: '46px', margin: '16px 0px' }} />}
                          </div>
                        )
                      }}
                    </Droppable>
                  </div>
                )
              })}
              <div className='builder__actions'>
                <button className='btn__primary--border' onClick={() => this.addList()}>
                  Agregar Tab
                </button>
                <button className='btn__primary--border btn__primary--alone' onClick={() => this.copyDialog()}>
                  Copiar diálogo
                </button>
                <button className='btn__primary' onClick={() => this.generateXML(this.state.items, this.state.itemsConfig)} disabled={this.state.items[1] < 0}>
                  Generar diálogo
                </button>
              </div>
            </LayoutRight>
          </DragDropContext>
        </Content>
        <Modal ariaHideApp={false} isOpen={this.state.modalIsOpen} style={styleModal()} contentLabel='Example Modal'>
          <div className='wrapperModal'>
            <div className='wrapperModal__body'>¿Está seguro que desea eliminar este item?</div>
            <div className='wrapperModal__actions'>
              <button className='btn__primary' onClick={() => this.onDeleteItem()}>
                Sí
              </button>
              <button className='btn__secondary' onClick={() => this.onCancelModal()}>
                No
              </button>
            </div>
          </div>
        </Modal>
      </React.Fragment>
    )
  }
}

Home.getInitialProps = async () => {
  resetServerContext()
  return { data: 'jeje' }
}

const mapStateToProps = (state) => ({ global: state })

const mapDispatchToProps = (dispatch) => {
  return {
    setMenuAction: (menu) => dispatch(setMenuAction(menu)),
    setPropertyType: (type, id) => dispatch(setPropertyType(type, id)),
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Home)
