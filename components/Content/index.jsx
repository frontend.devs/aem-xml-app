import React from 'react'
import { Provider } from 'react-redux'
import Head from 'next/head'
import store from '../../redux/store'
import '../../styles/styles.scss'

const Content = (props) => {
  return (
    <Provider store={store}>
      <Head>
        <meta name='viewport' content='width=device-width, initial-scale=1.0' />
        <title>AEM XML Builder</title>
      </Head>
      <div className='aem-content container'>
        {props.children}
      </div>
    </Provider>
  )
}

export default Content
