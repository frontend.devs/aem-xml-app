import React from 'react'
import { useDispatch, useSelector } from 'react-redux'
import { setMenuAction } from '../../redux/actions/navigation'
import {
  TYPE_CAMPOS,
  TYPE_PROPIEDADES,
  TYPE_DIALOGOS
} from '../../redux/types/navigation/types'

const Nav = () => {
  const dispatch = useDispatch()
  const activeMenu = useSelector(state => state.navigation.active)

  const onMenu = (menu) => dispatch(setMenuAction(menu))
  const isActive = (menu) => {
    return menu === activeMenu ? 'active' : ''
  }

  return (
    <nav className='nav-dialog'>
      <button
        onClick={() => onMenu(TYPE_CAMPOS)}
        className={isActive(TYPE_CAMPOS)}
      >
        Campos
      </button>
      <button
        onClick={() => onMenu(TYPE_PROPIEDADES)}
        className={isActive(TYPE_PROPIEDADES)}
      >
        Propiedades
      </button>
      <button
        onClick={() => onMenu(TYPE_DIALOGOS)}
        className={isActive(TYPE_DIALOGOS)}
      >
        Dialogos
      </button>
    </nav>
  )
}

export default Nav
