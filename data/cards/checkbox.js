const checkbox = `
<checkbox
    jcr:primaryType="nt:unstructured"
    sling:resourceType="granite/ui/components/foundation/form/checkbox"
    text="Yes"
    uncheckedValue="false"
    value="{Boolean}true"
    name="./checkbox"
/>`

export default checkbox
