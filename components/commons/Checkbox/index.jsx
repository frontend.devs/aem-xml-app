import React, { useState } from 'react'

function Checkbox(props) {
  const [checkbox] = useState({
    id: props.id,
    checked: props.checked,
    label: props.label,
    required: props.required,
  })

  return (
    <>
      <div className='input-checkbox'>
        <label className='container-checkbox'>
          {checkbox.label}
          <input type='checkbox' id={checkbox.id} name={checkbox.id} maxLength={props.maxLength} defaultChecked={checkbox.checked == 'true'} onChange={props.onChange} onBlur={props.handleBlur} onFocus={props.handleFocus} onKeyPress={props.onKeyPress ? handleKeyPress : null} />
          <span className='checkmark'></span>
        </label>
      </div>
    </>
  )
}

export default Checkbox
