import React from 'react'

const CardDialogPlaceholder = ({ data }) => {
  return (
    <div className='card-dialog' style={{ opacity: '.1' }}>
      <img src={data.img} alt={data.alt} />
      <p>{data.text}</p>
    </div>
  )
}

export default CardDialogPlaceholder
