import React, { useState } from 'react';

function TextField(props) {

	const [textfield,] = useState({
		id: props.id,
		type: props.type ,
		label: props.label,
		required: props.required ,
	})

	  

	const handleKeyDown = event => {
		let charCode = String.fromCharCode(event.which).toLowerCase();
		if (event.ctrlKey && charCode === "v") {
			event.preventDefault();
		}

		if (event.metaKey && charCode === "v") {
			event.preventDefault();
		}
	};

	const disableRightClickNumDoc = event => {
		event.preventDefault();
	};

	const handleKeyPress = event => {
		if (event.which === 13) {
			props.onSubmit(event);
		}
	};

	return (
		<>
			<div className="input-field"
				onKeyDown={props.onKeyDown ? handleKeyDown : null}
				onContextMenu={props.onContextMenu ? disableRightClickNumDoc : null} >
				<input
					className={props.validTextfield != '' ? 'error' : ''}
					id={textfield.id}
					type={props.type || textfield.type}
					name={textfield.id}
					maxLength={props.maxLength}
					value={props.value}
					onChange={props.onChange}
					onBlur={props.handleBlur}
					onFocus={props.handleFocus}
					onKeyPress={props.onKeyPress ? handleKeyPress : null}
				/>
				<label
					htmlFor={textfield.id}
					className={props.textfieldActive}>{textfield.label}
				</label>
				{props.enablePasswordIcon &&
					<i
						className={`eye-icon ${textfield.eye} ${
							props.type === "text"
								? "icon-eye-normal"
								: "icon-eye-pirata"
							}`}
						onClick={() => props.handlerEye()}></i>
				}
			</div>
			<span className="show-error">{textfield.required && props.validTextfield === '' ? '' : props.requiredMessage}</span>
		</>
	);
}

export default TextField;

