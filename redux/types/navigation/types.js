const COMMON = "NAVIGATION_";

export const SET_ACTIVE                  = COMMON + 'SET_ACTIVE';

export const TYPE_CAMPOS                 = 'CAMPOS'
export const TYPE_PROPIEDADES            = 'PROPIEDADES'
export const TYPE_DIALOGOS               = 'DIALOGOS'